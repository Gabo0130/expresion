/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

import Negocio.EvaluadorExpresion;

/**
 *
 * @author HP
 */
public class testCorrecta {
  public static void main(String[] args) {
        
        String uno = "(2+(3*4))";
        String dos = "((2+-39)*4)";
        String tres = "4*(5-2)/(1*9^3)";
        String cuatro = "43*(52-+32)/(12*92^32)";
        String cinco = "+3+5";
        String seis = "3-+5";
        String siete = "3**5";
        String ocho = "-3+5";
        EvaluadorExpresion eva = new EvaluadorExpresion();
        eva.setExpresion_infija("("+uno+")");
        System.out.println("infija : "+eva.getExpresion_infija());
        System.out.println("Es valida? : "+eva.EvaluarExpresion()+"\n");
        
        eva.setExpresion_infija("("+tres+")");
        System.out.println("infija : "+eva.getExpresion_infija());
        System.out.println("Es valida? : "+eva.EvaluarExpresion()+"\n");
        
        eva.setExpresion_infija("("+cinco+")");
        System.out.println("infija : "+eva.getExpresion_infija());
        System.out.println("Es valida? : "+eva.EvaluarExpresion()+"\n");
        
        eva.setExpresion_infija("("+seis+")");
        System.out.println("infija : "+eva.getExpresion_infija());
        System.out.println("Es valida? : "+eva.EvaluarExpresion()+"\n");
        
        eva.setExpresion_infija("("+siete+")");
        System.out.println("infija : "+eva.getExpresion_infija());
        System.out.println("Es valida? : "+eva.EvaluarExpresion()+"\n");
        
        eva.setExpresion_infija("("+ocho+")");
        System.out.println("infija : "+eva.getExpresion_infija());
        System.out.println("Es valida? : "+eva.EvaluarExpresion()+"\n");
        
        
        
        
    }
}
