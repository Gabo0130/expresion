/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.coleciones_seed;

/**
 *
 * @author madarme
 */
class NodoD<T> {
    
    private T info;
    private NodoD<T> siguiente, anterior;

    NodoD() {
    }

    NodoD(T info, NodoD<T> siguiente, NodoD<T> anterior) {
        this.info = info;
        this.siguiente = siguiente;
        this.anterior = anterior;
    }

    T getInfo() {
        return info;
    }

    void setInfo(T info) {
        this.info = info;
    }

    NodoD<T> getSiguiente() {
        return siguiente;
    }

    void setSiguiente(NodoD<T> siguiente) {
        this.siguiente = siguiente;
    }

    NodoD<T> getAnterior() {
        return anterior;
    }

    void setAnterior(NodoD<T> anterior) {
        this.anterior = anterior;
    }
    
    
    
    
    
}
