/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import ufps.util.coleciones_seed.Cola;
import ufps.util.coleciones_seed.Pila;

/**
 *
 * @author Ramiro Andres Jaimes && Jhony quintero 
 *
 */
public class EvaluadorExpresion {
    
    private String expresion_infija;

    public EvaluadorExpresion() {
    }

    public EvaluadorExpresion(String expresion_infija) {
        this.expresion_infija = expresion_infija;
    }

    public String getExpresion_infija() {
        return expresion_infija;
    }

    public void setExpresion_infija(String expresion_infija) {
        this.expresion_infija = expresion_infija;
    }

     /*
     Metodo de prueba para convertir la expresion en tokens by Jhony Quintero 
    */ 
   public String tokens(){
        Pila exp = new Pila();
        String aux="";
        String ord="";
    for(int i=0;i<this.expresion_infija.length();i++){
            char letra = this.expresion_infija.charAt(i);
            
            if(esOperador(letra+"")){
            exp.push(letra+",");
            i++;
            if(this.expresion_infija.charAt(i)=='-'||this.expresion_infija.charAt(i)=='+'){
             letra=this.expresion_infija.charAt(i);
            aux=letra+"";
            }else{
            i--;
            }
            }
            
            if(esNum(letra)){
                String msg=aux;
                char num=letra;
                while(!(esOperador(num+"")||num==')')){
                    num = this.expresion_infija.charAt(i);
                    if(!(esOperador(num+"")||num==')')){
                        msg=msg+num;
                        i++;
                    }
                    
                }
                exp.push(msg+",");
                i--;
                aux="";
            }
            if(letra==')'){
                exp.push(letra+",");
            }
            
     }
    for(int i=exp.getTamano()-1;i>=0;i--){
            String msg=exp.get(i)+"";
            ord=ord+msg;
            
        }

    return ord;
    }
    public boolean esOperador(char i) {

        boolean es = false;
        if (i == '^' || i == '-' || i == '/' || i == '*' || i == '+' || i == '(') {
            es = true;
        }
        return es;
    }

    public boolean esOperador(String i) {
        boolean es = false;
        if (i.equals("^") || i.equals("-") || i.equals("/") || i.equals("*")  || i.equals("+")) {
            es = true;
        }
        return es;
    }
    /*
    Devuelve si el char es un numero
    made by Jhony Qunitero
     */
    public boolean esNum(char i){
        
        boolean es = false;
        if(i=='0' || i=='1' || i=='2' || i=='3' || i=='4'|| i=='5'|| i=='6'|| i=='7'|| i=='8'|| i=='9'){
            es = true;
        }
        return es;
    }
      /*
    Metodo que se encarga de definir la prioridad la operacion
     */
    public int getPrioridad(String i) {

        int prioridad = 0;
        if (i.equals("-") || i.equals("+")) {
            prioridad = 1;
        } else if (i.equals("*") || i.equals("/")) {
            prioridad = 2;
        } else if (i.equals("^")) {
            prioridad = 3;
        }
        return prioridad;
    }

    
    public Cola ConvertirAToken() {

        Cola<String> tokens = new Cola();
        String aux = "";

        for (int i = 0; i < this.expresion_infija.length(); i++) {

            char letra = this.expresion_infija.charAt(i);

            if (esOperador(letra)) {
                tokens.enColar(letra+"");
                int tmp = i+1;
                if(this.expresion_infija.charAt(tmp)=='-' || this.expresion_infija.charAt(tmp)=='+'){
                    aux+=(this.expresion_infija.charAt(tmp)+"");
                    i++;
                }
            }else if (!esOperador(letra) && letra!=')') {
                aux += (letra);
                int tmp = i + 1;
                while (!esOperador(this.expresion_infija.charAt(tmp)) && this.expresion_infija.charAt(tmp) != ')') {
                    aux += (this.expresion_infija.charAt(tmp) + "");
                    tmp++;
                    i++;
                }
                tokens.enColar(aux);
                aux = "";
            }else if(letra==')'){
                tokens.enColar(letra+"");
            }
        }
        return tokens;
    }
    /*
    Metodo que se encarga de obtener el prefijo
    by ramiro jaimes
    
    */
   public String getPrefijo() {

        Pila<String> operadores = new Pila();
        Pila<String> operandos = new Pila();
        Cola<String> agrupados = this.ConvertirAToken();
        String prefijo = "";
        int tamano = agrupados.getTamano();

        for (int i = 0; i < tamano; i++) {
            
            String prueba = agrupados.consultar();
            
            if (prueba.equals("(")) {
                operadores.push(agrupados.deColar());
            } else if (prueba.equals(")")) {
                while (!operadores.esVacia() && !operadores.consultar().equals("(")) {
                    agrupados.deColar();
                    String numero1 = operandos.pop();
                    String numero2 = operandos.pop();
                    String operador = operadores.pop();
                    prefijo = (operador + "," + numero2 + "," + numero1);
                    operandos.push(prefijo);
                }
                operadores.pop();
            } else if (!esOperador(prueba)) {
                operandos.push(agrupados.deColar());
            } else {
                while (!operadores.esVacia() && getPrioridad(prueba)<=getPrioridad(operadores.consultar())) {
                    String numero1 = operandos.pop();
                    String numero2 = operandos.pop();
                    String operador = operadores.pop();
                    prefijo = (operador + "," + numero2 + "," + numero1);
                    operandos.push(prefijo);
                    
                }
                operadores.push(agrupados.deColar());
            }
        }
        
        while (!operadores.esVacia()) {
                String numero1 = operandos.pop();
                String numero2 = operandos.pop();
                String operador = operadores.pop();
                prefijo = (operador +","+ numero2 +","+ numero1);
                operandos.push(prefijo);
            }
        
        
        return operandos.pop();
    }
    
   /*
    Metodo que se encarga de obtener el postfijo
    Made By Jhony Quintero
     */
    public String getPostfijo(){
        Pila operadores = new Pila();
        Pila operandos = new Pila();
        String postfijo = "";
        String aux="";
        for(int i=0;i<this.expresion_infija.length();i++){
            char letra = this.expresion_infija.charAt(i);
            if(esOperador(letra)){
                operadores.push(letra);
                i++;
                if(this.expresion_infija.charAt(i)=='-'||this.expresion_infija.charAt(i)=='+'){
                    letra=this.expresion_infija.charAt(i);
                    aux=letra+"";
                }else{
                i--;
                }
            }else if(letra==')'){
                
                while(!operadores.getTope().equals('(')){
                    operandos.push(operadores.pop());
                }
                if(operadores.getTope().equals('(')){
                    operadores.pop();
                }
                if(!operadores.esVacia()&&!operadores.getTope().equals('(')){
                operandos.push(operadores.pop());
                }
                
                
            }else{
                if(esNum(letra)){
                String msg=aux;
                char num=letra;
                while(!(esOperador(num+"")||num==')')){
                    num = this.expresion_infija.charAt(i);
                    if(!(esOperador(num+"")||num==')')){
                        msg=msg+num;
                        i++;
                    }
                    
                }
                operandos.push(msg);
                i--;
                aux="";
            }
            }
        }
        for(int i=operandos.getTamano()-1;i>=0;i--){
            String msg=operandos.get(i)+",";
            postfijo=postfijo+msg;
            
        }
        
        return postfijo;
    }
    /*
    Metodo recibe dos numeros y un operador y devuelve el resultado de operar esos dos numeros
    */
    public float operar(float f, float F, String operador) {

        float resultado = 0.0f;

        if (operador.equals("*")) {
            resultado = f * F;

        } else if (operador.equals("/")) {
            resultado = f / F;

        } else if (operador.equals("+")) {
            resultado = f + F;

        } else if (operador.equals("-")) {
            resultado = f - F;

        } else if (operador.equals("^")) {
            resultado = (float) Math.pow(f, F);

        }

        return resultado * 1.0f;
    }
    public float getEvaluadorExpresion(){
       this.expresion_infija = this.getPrefijo();

        Pila<String> pila = new Pila();
        Pila<Float> tmp = new Pila();
        String[] arreglo = this.expresion_infija.split(",");
        //invierto la expresion que ya ha sido expresada de manera prefija
        for (int i = 0; i < arreglo.length; i++) {
            pila.push(arreglo[i]);
        }

        while (!pila.esVacia()) {

            if (esOperador(pila.getTope())) {
                float numero = tmp.pop();
                float numero2 = tmp.pop();
                String operador = pila.pop();
                float operacion = operar(numero, numero2, operador);
                tmp.push(operacion);

            } else {
                float push = Float.parseFloat(pila.pop());
                tmp.push(push);
            }
        }
        
        float Solve = tmp.pop();

        return Solve;
    }
    
    public  boolean  EvaluarExpresion(){
      boolean EsCorrecta=true;
        for(int i=0;i<this.expresion_infija.length();i++){
            char letra = this.expresion_infija.charAt(i);
            
            if(letra=='-'){
                i++;
                if(esOperador(this.expresion_infija.charAt(i))&&this.expresion_infija.charAt(i)!='('){
                    EsCorrecta=false;
                    i=this.expresion_infija.length();
                }else {
                i--;
                }
            }
            
            if(letra=='+'){
                i++;
                 if(esOperador(this.expresion_infija.charAt(i))&&this.expresion_infija.charAt(i)!='('&&this.expresion_infija.charAt(i)!='-'){
                    EsCorrecta=false;
                    i=this.expresion_infija.length();
                }else {
                i--;
                }
            }
            
            if(letra=='*'){
                i++;
               if(esOperador(this.expresion_infija.charAt(i))&&this.expresion_infija.charAt(i)!='('&&this.expresion_infija.charAt(i)!='-'){
                      EsCorrecta=false;
                    i=this.expresion_infija.length();
                }else {
                i--;
                }
            }
            
            if(letra=='/'){
                i++;
               if(esOperador(this.expresion_infija.charAt(i))&&this.expresion_infija.charAt(i)!='('&&this.expresion_infija.charAt(i)!='-'){
                      EsCorrecta=false;
                    i=this.expresion_infija.length();
                }else {
                i--;
                }
            }
            if(letra=='('){
                i++;
                if(esOperador(this.expresion_infija.charAt(i))&&this.expresion_infija.charAt(i)!='('&&this.expresion_infija.charAt(i)!='-'){
                     EsCorrecta=false;
                    i=this.expresion_infija.length();
                }else {
                i--;
                }
            }
            if(letra==')'){
                i--;
                if(esOperador(this.expresion_infija.charAt(i))){
                     EsCorrecta=false;
                    i=this.expresion_infija.length();
                }else {
                i++;
                }
                
                    
            
            }
            if(!(esOperador(letra)||letra==')'||esNum(letra))){
                EsCorrecta=false;
                i=this.expresion_infija.length();
            
            }
        }
       return EsCorrecta;  
    }
    
    public void EvaluarExpresion(String nuevaExp){
        
    }
    
    
}
